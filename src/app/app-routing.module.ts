import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditPageComponentComponent } from './components/edit-page-component/edit-page-component.component';
import {AboutUsContentComponent} from '../app/components/about-us-content/about-us-content.component';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { EditPageTextContentComponent } from './components/edit-page-text-content/edit-page-text-content.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { SupportUsContentComponent } from './components/support-us-content/support-us-content.component';
import { EditResourceCardContentComponent } from './components/edit-resource-card-content/edit-resource-card-content.component';


const routes: Routes = [
  { path: 'Home', component: HomeContentComponent},
  { path: 'AboutUs', component: AboutUsContentComponent},
  { path: 'SupportUs', component: SupportUsContentComponent},
  { path: 'AboutUs/:pageContentCode', component: EditPageComponentComponent},
  { path: 'SupportUs/:resourceContentCode', component: EditResourceCardContentComponent},
  { path: 'Home/Cards/:cardCode', component: EditCardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
