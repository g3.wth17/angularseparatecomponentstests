import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceContentService } from 'src/app/services/resource-content.service';
import { ResourceContent } from 'src/app/models/ResourceContent';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-resource-card-content',
  templateUrl: './edit-resource-card-content.component.html',
  styleUrls: ['./edit-resource-card-content.component.css']
})
export class EditResourceCardContentComponent implements OnInit {
newTitle: string;
newDescription: string;
newImageUrl: string;
oldContent: ResourceContent;
newContent: ResourceContent;
  constructor(private route:ActivatedRoute, private resourceCardService: ResourceContentService, private router:Router) { }

  ngOnInit(): void {
    const contentCode = this.route.snapshot.paramMap.get("resourceContentCode");
    this.resourceCardService.getResource(contentCode).subscribe(
      content => {
        this.oldContent = content;
      }
    )
  }

  onSubmit(){
    const contentCode = this.route.snapshot.paramMap.get("resourceContentCode");
    this.newContent = {
      code: this.oldContent.code,
      title: this.newTitle,
      description: this.newDescription,
      imageUrl: this.newImageUrl,
      resources: this.oldContent.resources
    }
    this.resourceCardService.updateResources(contentCode, this.newContent).subscribe(
      content=> {
        console.log("Modificado");
      }
    );
    this.router.navigateByUrl('SupportUs');
  }

  onStepBack(){
    this.router.navigateByUrl('SupportUs');
  }

}
