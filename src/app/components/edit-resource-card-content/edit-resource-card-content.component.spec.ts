import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditResourceCardContentComponent } from './edit-resource-card-content.component';

describe('EditResourceCardContentComponent', () => {
  let component: EditResourceCardContentComponent;
  let fixture: ComponentFixture<EditResourceCardContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditResourceCardContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditResourceCardContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
