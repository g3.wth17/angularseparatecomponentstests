import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-facilitator',
  templateUrl: './add-facilitator.component.html',
  styleUrls: ['./add-facilitator.component.css']
})
export class AddFacilitatorComponent implements OnInit {
  name:string;
  lastName: string;
  cellphoneNumber: number;
  country: string;
  emailAddress: string;
  speciality: string;
  @Output() addFacilitator: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const facilitator = {
      name: this.name,
      lastName: this.lastName,
      cellphoneNumber: this.cellphoneNumber,
      country: this.country,
      emailAddress: this.emailAddress,
      speciality: this.speciality
    };
    this.addFacilitator.emit(facilitator);
  }

}
