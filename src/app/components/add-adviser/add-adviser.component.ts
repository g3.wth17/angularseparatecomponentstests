import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-add-adviser',
  templateUrl: './add-adviser.component.html',
  styleUrls: ['./add-adviser.component.css']
})
export class AddAdviserComponent implements OnInit {
  name:string;
  lastName: string;
  subject:string;
  cellphoneNumber: number;
  emailAddress: string;
  @Output() addAdviser: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const adviser = {
      name: this.name, 
      lastName: this.lastName,
      subject: this.subject,
      cellphoneNumber: this.cellphoneNumber,
      emailAddress: this.emailAddress
    };
    this.addAdviser.emit(adviser);
  }

}
