import { Component, OnInit, Input } from '@angular/core';
import { Mentor } from '../../models/Mentor';

@Component({
  selector: 'app-mentor-item',
  templateUrl: './mentor-item.component.html',
  styleUrls: ['./mentor-item.component.css']
})
export class MentorItemComponent implements OnInit {
  @Input() mentor:Mentor;
  constructor() { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      mentor: true
    }
    return classes;
  }

}
