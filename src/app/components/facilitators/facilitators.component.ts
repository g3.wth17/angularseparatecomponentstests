import { Component, OnInit } from '@angular/core';
import { Facilitator } from '../../models/Facilitator';
import { FacilitatorService } from '../../services/facilitator.service';
@Component({
  selector: 'app-facilitators',
  templateUrl: './facilitators.component.html',
  styleUrls: ['./facilitators.component.css']
})
export class FacilitatorsComponent implements OnInit {
facilitators: Facilitator[];
  constructor(private facilitatorService: FacilitatorService) { }

  ngOnInit(): void {
    this.facilitatorService.getFacilitators().subscribe(
      facilitators => {
        this.facilitators = facilitators;
      }
    )
  }

  addFacilitator(facilitator: Facilitator){
    this.facilitatorService.addFacilitator(facilitator).subscribe(
      facilitator => {
        this.facilitators.push(facilitator);
      } 
    );
  }

}
