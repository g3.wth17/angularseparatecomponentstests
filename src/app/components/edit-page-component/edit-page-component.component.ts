import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageContent } from 'src/app/models/PageContent';
import { AboutUsContentService } from 'src/app/services/about-us-content.service';

@Component({
  selector: 'app-edit-page-component',
  templateUrl: './edit-page-component.component.html',
  styleUrls: ['./edit-page-component.component.css']
})
export class EditPageComponentComponent implements OnInit {
newTitle: string;
newContent : string;
oldContent: PageContent;
newPageContent: PageContent;
  constructor(private route:ActivatedRoute, private aboutUsService: AboutUsContentService, private router:Router) { }

  ngOnInit(): void {
    const contentCode = this.route.snapshot.paramMap.get("pageContentCode");
    this.aboutUsService.getSpecificContent(contentCode).subscribe( content =>
      {
        this.oldContent = content;
      });
  }

  onSubmit(){
    const contentCode = this.route.snapshot.paramMap.get("pageContentCode");
    this.newPageContent={
      code: this.oldContent.code,
      title: this.newTitle,
      content: this.newContent,
      imageUrl: this.oldContent.imageUrl
    }
    this.aboutUsService.putContent(contentCode, this.newPageContent).subscribe(
      content=> {
        console.log("Modificado");
      }
    );
    this.router.navigateByUrl('AboutUs');
  }

  onStepBack(){
    this.router.navigateByUrl('AboutUs');
  }

}
