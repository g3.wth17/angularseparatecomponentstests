import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPageComponentComponent } from './edit-page-component.component';

describe('EditPageComponentComponent', () => {
  let component: EditPageComponentComponent;
  let fixture: ComponentFixture<EditPageComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPageComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPageComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
