import { Component, OnInit, Input } from '@angular/core';
import { Facilitator } from '../../models/Facilitator';

@Component({
  selector: 'app-facilitator-item',
  templateUrl: './facilitator-item.component.html',
  styleUrls: ['./facilitator-item.component.css']
})
export class FacilitatorItemComponent implements OnInit {

  @Input() facilitator: Facilitator;
  constructor() { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      facilitator:true
    }
    return classes;
  }
}
