import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitatorItemComponent } from './facilitator-item.component';

describe('FacilitatorItemComponent', () => {
  let component: FacilitatorItemComponent;
  let fixture: ComponentFixture<FacilitatorItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitatorItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitatorItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
