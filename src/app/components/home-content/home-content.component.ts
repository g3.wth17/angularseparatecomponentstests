import { Component, OnInit } from '@angular/core';
import { PageContent } from '../../models/PageContent';
import { HomeContentService} from '../../services/home-content.service';
import { CardContent } from 'src/app/models/CardContent';

@Component({
  selector: 'app-home-content',
  templateUrl: './home-content.component.html',
  styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent implements OnInit {
cards: CardContent[];
  constructor(private contentService: HomeContentService) { 
   this.ngOnInit(); 
  }

  ngOnInit(): void {
    
  }

}
