import { Component, OnInit } from '@angular/core';
import { Volunteer } from '../../models/Volunteer';
import { VolunteerService } from '../../services/volunteer.service';

@Component({
  selector: 'app-volunteers',
  templateUrl: './volunteers.component.html',
  styleUrls: ['./volunteers.component.css']
})
export class VolunteersComponent implements OnInit {
volunteers:Volunteer[];
  constructor(private volunteerService: VolunteerService) { }

  ngOnInit(): void {
    this.volunteerService.getVolunteers().subscribe( 
      volunteers => {
        this.volunteers = volunteers;
      } )
  }

  addVolunteer(volunteer: Volunteer){
    this.volunteerService.addVolunteer(volunteer).subscribe(
      volunteer =>{
        this.volunteers.push(volunteer);
      }
    );
  }

}
