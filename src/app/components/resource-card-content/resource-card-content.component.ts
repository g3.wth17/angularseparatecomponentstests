import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ResourceContent } from 'src/app/models/ResourceContent';
import { Router, ActivatedRoute } from '@angular/router';
import { Pair } from 'src/app/models/Pair';
import { PairContentComponent } from '../pair-content/pair-content.component';
import { PairContentService } from 'src/app/services/pair-content.service';

@Component({
  selector: 'app-resource-card-content',
  templateUrl: './resource-card-content.component.html',
  styleUrls: ['./resource-card-content.component.css']
})
export class ResourceCardContentComponent implements OnInit {
@Input() resourceContent: ResourceContent;
@Output() addResource: EventEmitter<any> = new EventEmitter();
@Output() refreshPage: EventEmitter<boolean> = new EventEmitter();
actualPair: Pair;
resource: string;
  constructor(private router: Router, private pairService: PairContentService) { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes={
      resourceContent :true
    }
    return classes;
  }

  onEdit(){
    this.router.navigateByUrl(`SupportUs/${this.resourceContent.code}`);
  }

  onSubmit(){
    const resource = {
      code: this.resourceContent.code,
      resource: this.resource
    }

    this.addResource.emit(resource);

  }

  getPair(pair:Pair){
    this.actualPair = pair;
    console.log("Obtenido");
  }

  deletePair(pair: Pair){
    this.pairService.deletePair(pair.code, pair.resource).subscribe(
      revisor => {
        console.log("Eliminado");
        this.refreshPage.emit(true);
      }
    )
  }

  editPair(pair:Pair){
    this.pairService.updatePair(this.actualPair.code, this.actualPair.resource, pair).subscribe(
      pair=> {
        console.log("Modificado");
        this.refreshPage.emit(true);
      }
    )
  }

}
