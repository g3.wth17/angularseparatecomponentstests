import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceCardContentComponent } from './resource-card-content.component';

describe('ResourceCardContentComponent', () => {
  let component: ResourceCardContentComponent;
  let fixture: ComponentFixture<ResourceCardContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceCardContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceCardContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
