import { Component, OnInit, Input } from '@angular/core';
import { Volunteer } from '../../models/Volunteer';

@Component({
  selector: 'app-volunteer-item',
  templateUrl: './volunteer-item.component.html',
  styleUrls: ['./volunteer-item.component.css']
})
export class VolunteerItemComponent implements OnInit {
@Input() volunteer:Volunteer;
  constructor() { }

  ngOnInit(): void {
  }
  setClasses(){
    let classes = {
      volunteer:true
    }
    return classes;
  }
}
