import { Component, OnInit, Input } from '@angular/core';
import { PageContent } from '../../models/PageContent';
import { Router } from '@angular/router';
@Component({
  selector: 'app-page-component-item',
  templateUrl: './page-component-item.component.html',
  styleUrls: ['./page-component-item.component.css']
})
export class PageComponentItemComponent implements OnInit {
  @Input() pageContent:PageContent;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      pageContent : true
    }
    return classes;
  }

  onEdit(){
    this.router.navigateByUrl(`AboutUs/${this.pageContent.code}`);
  }
}
