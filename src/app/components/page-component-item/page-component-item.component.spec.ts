import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageComponentItemComponent } from './page-component-item.component';

describe('PageComponentItemComponent', () => {
  let component: PageComponentItemComponent;
  let fixture: ComponentFixture<PageComponentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageComponentItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageComponentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
