import { Component, OnInit } from '@angular/core';
import { Mentor } from '../../models/Mentor';
import { MentorService } from '../../services/mentor.service';
@Component({
  selector: 'app-mentors',
  templateUrl: './mentors.component.html',
  styleUrls: ['./mentors.component.css']
})
export class MentorsComponent implements OnInit {
mentors: Mentor[];
  constructor(private mentorService: MentorService) { }

  ngOnInit(): void {
    this.mentorService.getMentors().subscribe(
      mentors => {
        this.mentors = mentors;
      }
    )
  }

  addMentor(mentor:Mentor){
    this.mentorService.addMentor(mentor).subscribe(
      mentor => {
        this.mentors.push(mentor);
      }
    );
  }

}
