import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-mentor',
  templateUrl: './add-mentor.component.html',
  styleUrls: ['./add-mentor.component.css']
})
export class AddMentorComponent implements OnInit {
  name:string;
  lastName: string;
  cellphoneNumber: number;
  country: string;
  emailAddress: string;
  @Output() addMentor: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const mentor = {
      name: this.name,
      lastName: this.lastName,
      cellphoneNumber: this.cellphoneNumber,
      country: this.country,
      emailAddress: this.emailAddress
    };
    this.addMentor.emit(mentor);
  }

}
