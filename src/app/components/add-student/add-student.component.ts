import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  name:string;
  lastName: string;
  school: string;
  userName: string;
  birthDate: DatePipe;
  genre: number;
  @Output() addStudent: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const student = {
      name: this.name,
      lastName: this.lastName,
      school: this.school,
      userName: this.userName,
      birthDate: this.birthDate,
      genre: this.genre
    };
    this.addStudent.emit(student);
  }

}
