import { Component, OnInit } from '@angular/core';
import {Adviser} from '../../models/Adviser';
import { AdviserService } from '../../services/adviser.service';

@Component({
  selector: 'app-advisers',
  templateUrl: './advisers.component.html',
  styleUrls: ['./advisers.component.css']
})
export class AdvisersComponent implements OnInit {
advisers:Adviser[];
  constructor(private adviserService: AdviserService) { }

  ngOnInit(): void {
    this.adviserService.getAdvisers().subscribe(
      advisers => {
        this.advisers = advisers;
      }
    )
  }

  addAdviser(adviser:Adviser){
    this.adviserService.addAdviser(adviser).subscribe(
      adviser => {
        this.advisers.push(adviser);
      }
    );
  }

}
