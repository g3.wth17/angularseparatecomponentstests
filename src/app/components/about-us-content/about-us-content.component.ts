import { Component, OnInit } from '@angular/core';
import { PageContent } from '../../models/PageContent';
import { AboutUsContentService }from '../../services/about-us-content.service'

@Component({
  selector: 'app-about-us-content',
  templateUrl: './about-us-content.component.html',
  styleUrls: ['./about-us-content.component.css']
})
export class AboutUsContentComponent implements OnInit {
contents: PageContent[];
  constructor(private contentService: AboutUsContentService) { this.ngOnInit() }

  ngOnInit(): void {
    this.contentService.getContent().subscribe(
      contents => {
        this.contents = contents;
      }
    )

  }

}
