import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-add-volunteer',
  templateUrl: './add-volunteer.component.html',
  styleUrls: ['./add-volunteer.component.css']
})
export class AddVolunteerComponent implements OnInit {
  name:string;
  lastName: string;
  cellphoneNumber: number;
  emailAddress: string;
  @Output() addVolunteer: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const volunteer = {
      name :this.name,
      lastName: this.lastName,
      cellphoneNumber : this.cellphoneNumber,
      emailAddress: this.emailAddress
    };
    this.addVolunteer.emit(volunteer);
  }

}
