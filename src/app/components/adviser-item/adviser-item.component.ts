import { Component, OnInit, Input } from '@angular/core';
import { Adviser } from 'src/app/models/Adviser';

@Component({
  selector: 'app-adviser-item',
  templateUrl: './adviser-item.component.html',
  styleUrls: ['./adviser-item.component.css']
})
export class AdviserItemComponent implements OnInit {
  @Input() adviser:Adviser;
  constructor() { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      adviser: true
    }
    return classes;
  }

}
