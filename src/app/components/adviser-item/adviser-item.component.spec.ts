import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdviserItemComponent } from './adviser-item.component';

describe('AdviserItemComponent', () => {
  let component: AdviserItemComponent;
  let fixture: ComponentFixture<AdviserItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdviserItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdviserItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
