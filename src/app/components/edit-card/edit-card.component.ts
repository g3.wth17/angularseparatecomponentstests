import { Component, OnInit } from '@angular/core';
import { CardContent } from 'src/app/models/CardContent';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeContentService } from 'src/app/services/home-content.service';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css']
})
export class EditCardComponent implements OnInit {
newTitle: string;
backColor: string;
newImageUrl: string;
oldCard: CardContent;
newCard: CardContent;
  constructor(private route: ActivatedRoute, private homeService: HomeContentService, private router: Router) { }

  ngOnInit(): void {
    const contentCode = this.route.snapshot.paramMap.get("cardCode");
    this.homeService.getSpecificCardContent(contentCode).subscribe( content=>{
      this.oldCard = content;
    })
  }

  onSubmit(){
    const contentCode = this.route.snapshot.paramMap.get("cardCode");
    this.newCard = {
      code: this.oldCard.code,
      title: this.newTitle,
      imageUrl: this.newImageUrl,
      backgroundColor: this.backColor
    }
    this.homeService.putCardContent(contentCode, this.newCard).subscribe(
      content=>{
        console.log("Modificado");
      }
    )
    this.router.navigateByUrl('Home');
  }

  onStepBack(){
    this.router.navigateByUrl('Home');
  }

}
