import { Component, OnInit, Input } from '@angular/core';
import { Student } from '../../models/Student';
import { StudentsComponent } from '../students/students.component';
import { generate } from 'rxjs';

@Component({
  selector: 'app-student-item',
  templateUrl: './student-item.component.html',
  styleUrls: ['./student-item.component.css']
})
export class StudentItemComponent implements OnInit {
  @Input() student:Student;
  constructor() { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      student: true,
    }
    return classes;
  }

  defineGenre(): string{
    if(this.student.genre == 1)
      return "Male";
    else
      return "Female";
  }

}
