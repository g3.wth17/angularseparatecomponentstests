import { Component, OnInit } from '@angular/core';
import { PageContent } from 'src/app/models/PageContent';
import { HomeContentService } from 'src/app/services/home-content.service';

@Component({
  selector: 'app-home-text-content',
  templateUrl: './home-text-content.component.html',
  styleUrls: ['./home-text-content.component.css']
})
export class HomeTextContentComponent implements OnInit {
  contents: PageContent[];
  constructor(private contentService: HomeContentService) {this.ngOnInit() }

  ngOnInit(): void {
    this.contentService.getTextContent().subscribe(
      contents =>{
        this.contents = contents;
      }
    )
  }

}
