import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTextContentComponent } from './home-text-content.component';

describe('HomeTextContentComponent', () => {
  let component: HomeTextContentComponent;
  let fixture: ComponentFixture<HomeTextContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTextContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTextContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
