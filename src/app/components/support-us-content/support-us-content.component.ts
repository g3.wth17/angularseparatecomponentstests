import { Component, OnInit } from '@angular/core';
import { ResourceContent } from 'src/app/models/ResourceContent';
import { ResourceContentService } from 'src/app/services/resource-content.service';
import { Pair } from 'src/app/models/Pair';
import { PairContentService } from 'src/app/services/pair-content.service';

@Component({
  selector: 'app-support-us-content',
  templateUrl: './support-us-content.component.html',
  styleUrls: ['./support-us-content.component.css']
})
export class SupportUsContentComponent implements OnInit {
resources: ResourceContent[];
  constructor(private resourcesService: ResourceContentService, private pairService: PairContentService) { }

  ngOnInit(): void {
    this.resourcesService.getResources().subscribe(
      resources=> {
        this.resources = resources;
      }
    )
  }

  addResource(resource: Pair){
    this.pairService.postPair(resource.code, resource).subscribe(
      resource => {
        console.log("Incluido");
        this.ngOnInit();
      }
    )
  }

  refreshPage(answer: boolean){
    this.ngOnInit();
  }


}
