import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportUsContentComponent } from './support-us-content.component';

describe('SupportUsContentComponent', () => {
  let component: SupportUsContentComponent;
  let fixture: ComponentFixture<SupportUsContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportUsContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportUsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
