import { Component, OnInit, Input } from '@angular/core';
import { PageContent } from '../../models/PageContent';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-text-content',
  templateUrl: './page-text-content.component.html',
  styleUrls: ['./page-text-content.component.css']
})
export class PageTextContentComponent implements OnInit {
  @Input() pageTextContent: PageContent;
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      pageTextContent: true
    }
    return classes;
  }

  onEdit(){
    this.router.navigateByUrl(`Home/${this.pageTextContent.code}`); 
  }

}
