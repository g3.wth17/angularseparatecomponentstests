import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTextContentComponent } from './page-text-content.component';

describe('PageTextContentComponent', () => {
  let component: PageTextContentComponent;
  let fixture: ComponentFixture<PageTextContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTextContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTextContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
