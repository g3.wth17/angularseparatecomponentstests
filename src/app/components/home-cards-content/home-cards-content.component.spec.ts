import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCardsContentComponent } from './home-cards-content.component';

describe('HomeCardsContentComponent', () => {
  let component: HomeCardsContentComponent;
  let fixture: ComponentFixture<HomeCardsContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCardsContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCardsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
