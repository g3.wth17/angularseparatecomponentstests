import { Component, OnInit } from '@angular/core';
import { HomeContentService } from 'src/app/services/home-content.service';
import { CardContent } from 'src/app/models/CardContent';

@Component({
  selector: 'app-home-cards-content',
  templateUrl: './home-cards-content.component.html',
  styleUrls: ['./home-cards-content.component.css']
})
export class HomeCardsContentComponent implements OnInit {
  cards: CardContent[];
  constructor(private contentService: HomeContentService) { this.ngOnInit()}

  ngOnInit(): void {
    this.contentService.getCardsContent().subscribe(
      cards =>{
        this.cards = cards;
      }
    )
  }

}
