import { Component, OnInit, Input } from '@angular/core';
import { CardContent } from 'src/app/models/CardContent';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.css']
})
export class CardItemComponent implements OnInit {
@Input() cardContent: CardContent;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      cardContent: true
    }
    return classes;
  }

  onEdit(){
    this.router.navigateByUrl(`Home/Cards/${this.cardContent.code}`);
  }

}
