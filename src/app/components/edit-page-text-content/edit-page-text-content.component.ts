import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageContent } from 'src/app/models/PageContent';
import { HomeContentService} from '../../services/home-content.service';

@Component({
  selector: 'app-edit-page-text-content',
  templateUrl: './edit-page-text-content.component.html',
  styleUrls: ['./edit-page-text-content.component.css']
})
export class EditPageTextContentComponent implements OnInit {
  newContent: string;
  oldContent: PageContent;
  newTextContent:PageContent;

  constructor(private route:ActivatedRoute, private homeService: HomeContentService, private router:Router) { }

  ngOnInit(): void {
    const contentCode = this.route.snapshot.paramMap.get("pageTextContentCode");
    this.homeService.getSpecificTextContent(contentCode).subscribe( content =>{
      this.oldContent = content;
    });
  }

  onSubmit(){
    const contentCode = this.route.snapshot.paramMap.get("pageTextContentCode");
    this.newTextContent={
      code: this.oldContent.code,
      title: this.oldContent.title,
      content: this.newContent,
      imageUrl: this.oldContent.imageUrl
    }
    this.homeService.putTextcontent(contentCode, this.newTextContent).subscribe(
      content=> {
        console.log("Modificado");
      }
    );
    this.router.navigateByUrl('Home');
  }

  onStepBack(){
    this.router.navigateByUrl('Home');
  }

}
