import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPageTextContentComponent } from './edit-page-text-content.component';

describe('EditPageTextContentComponent', () => {
  let component: EditPageTextContentComponent;
  let fixture: ComponentFixture<EditPageTextContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPageTextContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPageTextContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
