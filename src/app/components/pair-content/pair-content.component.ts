import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pair } from 'src/app/models/Pair';

@Component({
  selector: 'app-pair-content',
  templateUrl: './pair-content.component.html',
  styleUrls: ['./pair-content.component.css']
})
export class PairContentComponent implements OnInit {
  newContent: string;
  newPair: Pair;
@Input() pairContent: Pair;
@Output() deletePair: EventEmitter<Pair> = new EventEmitter();
@Output() editPair: EventEmitter<Pair> = new EventEmitter();
@Output() getPair: EventEmitter<Pair> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  setClasses(){
    let classes = {
      pairContent :true
    }
    return classes;
  }

  onEdit(){
    this.getPair.emit(this.pairContent);
    this.newPair = {
      code: this.pairContent.code,
      resource: this.newContent
    }
    this.editPair.emit(this.newPair);
  }
  
  onDelete(){
    this.deletePair.emit(this.pairContent);
  }

}
