import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PairContentComponent } from './pair-content.component';

describe('PairContentComponent', () => {
  let component: PairContentComponent;
  let fixture: ComponentFixture<PairContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PairContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PairContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
