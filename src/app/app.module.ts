import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

import { StudentsComponent } from './components/students/students.component';
import { StudentItemComponent } from './components/student-item/student-item.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { AdvisersComponent } from './components/advisers/advisers.component';
import { AdviserItemComponent } from './components/adviser-item/adviser-item.component';
import { VolunteersComponent } from './components/volunteers/volunteers.component';
import { VolunteerItemComponent } from './components/volunteer-item/volunteer-item.component';
import { MentorItemComponent } from './components/mentor-item/mentor-item.component';
import { MentorsComponent } from './components/mentors/mentors.component';
import { FacilitatorItemComponent } from './components/facilitator-item/facilitator-item.component';
import { FacilitatorsComponent } from './components/facilitators/facilitators.component';
import { AddStudentComponent } from './components/add-student/add-student.component';
import { AddAdviserComponent } from './components/add-adviser/add-adviser.component';
import { AddVolunteerComponent } from './components/add-volunteer/add-volunteer.component';
import { AddMentorComponent } from './components/add-mentor/add-mentor.component';
import { AddFacilitatorComponent } from './components/add-facilitator/add-facilitator.component';
import { AboutUsContentComponent } from './components/about-us-content/about-us-content.component';
import { PageComponentItemComponent } from './components/page-component-item/page-component-item.component';
import { EditPageComponentComponent } from './components/edit-page-component/edit-page-component.component';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { PageTextContentComponent } from './components/page-text-content/page-text-content.component';
import { EditPageTextContentComponent } from './components/edit-page-text-content/edit-page-text-content.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { HomeTextContentComponent } from './components/home-text-content/home-text-content.component';
import { HomeCardsContentComponent } from './components/home-cards-content/home-cards-content.component';
import { ResourceCardContentComponent } from './components/resource-card-content/resource-card-content.component';
import { SupportUsContentComponent } from './components/support-us-content/support-us-content.component';
import { PairContentComponent } from './components/pair-content/pair-content.component';
import { EditResourceCardContentComponent } from './components/edit-resource-card-content/edit-resource-card-content.component';


@NgModule({
  declarations: [
    AppComponent, 
    StudentsComponent,
    StudentItemComponent,
    HeaderComponent,
    AdvisersComponent,
    AdviserItemComponent,
    VolunteersComponent,
    VolunteerItemComponent,
    MentorItemComponent,
    MentorsComponent,
    FacilitatorItemComponent,
    FacilitatorsComponent,
    AddStudentComponent,
    AddAdviserComponent,
    AddVolunteerComponent,
    AddMentorComponent,
    AddFacilitatorComponent,
    AboutUsContentComponent,
    PageComponentItemComponent,
    EditPageComponentComponent,
    HomeContentComponent,
    PageTextContentComponent,
    EditPageTextContentComponent,
    CardItemComponent,
    EditCardComponent,
    HomeTextContentComponent,
    HomeCardsContentComponent,
    ResourceCardContentComponent,
    SupportUsContentComponent,
    PairContentComponent,
    EditResourceCardContentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
