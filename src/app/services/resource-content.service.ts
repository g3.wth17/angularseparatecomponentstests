import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResourceContent } from '../models/ResourceContent';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ResourceContentService {
  resourcesUrl: string = "http://localhost:50355/api/SupportUs";
  constructor(private http: HttpClient) { }

  getResources(): Observable<ResourceContent[]>{
    return this.http.get<ResourceContent[]>(`${this.resourcesUrl}`);
  }

  getResource(code: string): Observable<ResourceContent>{
    return this.http.get<ResourceContent>(`${this.resourcesUrl}/${code}`);
  }

  updateResources(code: string, resource: ResourceContent): Observable<ResourceContent>{
    return this.http.put<ResourceContent>(`${this.resourcesUrl}/${code}`, resource, httpOptions);
  }
}
