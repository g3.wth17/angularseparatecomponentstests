import { TestBed } from '@angular/core/testing';

import { AboutUsContentService } from './about-us-content.service';

describe('AboutUsContentService', () => {
  let service: AboutUsContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AboutUsContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
