import { TestBed } from '@angular/core/testing';

import { PairContentService } from './pair-content.service';

describe('PairContentService', () => {
  let service: PairContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PairContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
