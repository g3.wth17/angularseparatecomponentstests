import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageContent } from '../models/PageContent';
import { CardContent } from '../models/CardContent';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class HomeContentService {
  homeContentUrl: string = "http://localhost:50355/api/Home"
  constructor(private http: HttpClient) { 
  }

  getTextContent():Observable<PageContent[]>{
    return this.http.get<PageContent[]>(`${this.homeContentUrl}/Text`);
  }

  getCardsContent(): Observable<CardContent[]>{
    return this.http.get<CardContent[]>(`${this.homeContentUrl}/Cards`);
  }

  getSpecificTextContent(code:string):Observable<PageContent>{
    return this.http.get<PageContent>(`${this.homeContentUrl}/Text/${code}`);
  }

  getSpecificCardContent(code:string): Observable<CardContent>{
    return this.http.get<CardContent>(`${this.homeContentUrl}/Cards/${code}`);
  }

  putTextcontent(code: string, content: PageContent):Observable<PageContent>{
    return this.http.put<PageContent>(`${this.homeContentUrl}/Text/${code}`,content, httpOptions);
  }

  putCardContent(code: string, card: CardContent): Observable<CardContent>{
    return this.http.put<CardContent>(`${this.homeContentUrl}/Cards/${code}`,card, httpOptions);
  }
}
