import { Injectable } from '@angular/core';
import { Student } from '../models/Student';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  studentsUrl: string = 'http://localhost:50568/api/students';
  constructor(private http: HttpClient) { }

  //Get Students
  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>(`${this.studentsUrl}`);
  }

  addStudent(student: Student): Observable<Student>{
    return this.http.post<Student>(this.studentsUrl, student, httpOptions);
  }
}
