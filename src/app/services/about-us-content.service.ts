import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageContent } from '../models/PageContent';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}


@Injectable({
  providedIn: 'root'
})
export class AboutUsContentService {
  aboutUsContentUrl: string = "http://localhost:50355/api/AboutUs";
  constructor(private http: HttpClient) { }

  //Get all request
  getContent(): Observable<PageContent[]>{
    return this.http.get<PageContent[]>(`${this.aboutUsContentUrl}`);
  }

  getSpecificContent(code:string):Observable<PageContent>{
    return this.http.get<PageContent>(`${this.aboutUsContentUrl}/${code}`);
  }

  putContent(code:string, content: PageContent):Observable<PageContent>{
    return this.http.put<PageContent>(`${this.aboutUsContentUrl}/${code}`, content, httpOptions);
  }
}
