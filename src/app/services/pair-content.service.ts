import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pair } from '../models/Pair';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class PairContentService {
  pairResources: string = "http://localhost:50355/api/SupportUs"
  constructor(private http: HttpClient) { }

  postPair(code: string, pair: Pair):Observable<Pair>{
    return this.http.post<Pair>(`${this.pairResources}/${code}/Pair`,pair, httpOptions);
  }

  deletePair(code: string, resource: string): Observable<boolean>{
    return this.http.delete<boolean>(`${this.pairResources}/${code}/Pair/${resource}`, httpOptions);
  }

  updatePair(code: string, resource: string, pair: Pair): Observable<Pair>{
    return this.http.put<Pair>(`${this.pairResources}/${code}/Pair/${resource}`, pair, httpOptions);
  }
}
