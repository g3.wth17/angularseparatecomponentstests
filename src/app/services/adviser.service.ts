import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Adviser } from '../models/Adviser';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class AdviserService {
  advisersUrl: string = 'http://localhost:50568/api/advisers';
  constructor(private http: HttpClient) { }

  //Get Request
  getAdvisers(): Observable<Adviser[]>{
    return this.http.get<Adviser[]>(`${this.advisersUrl}`);
  }

  addAdviser(adviser: Adviser):Observable<Adviser>{
    return this.http.post<Adviser>(this.advisersUrl, adviser, httpOptions);
  }
}
