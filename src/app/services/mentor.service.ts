import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mentor } from '../models/Mentor';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class MentorService {
  mentorsUrl: string = 'http://localhost:50568/api/mentors'
  constructor(private http: HttpClient) { }

  getMentors(): Observable<Mentor[]>{
    return this.http.get<Mentor[]>(`${this.mentorsUrl}`);
  }

  addMentor(mentor: Mentor): Observable<Mentor>{
    return this.http.post<Mentor>(this.mentorsUrl, mentor, httpOptions);
  }
}
