import { TestBed } from '@angular/core/testing';

import { ResourceContentService } from './resource-content.service';

describe('ResourceContentService', () => {
  let service: ResourceContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResourceContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
