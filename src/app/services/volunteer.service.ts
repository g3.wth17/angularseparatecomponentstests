import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Volunteer} from '../models/Volunteer';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class VolunteerService {
  volunteersUrl: string = 'http://localhost:50568/api/volunteers'
  constructor(private http: HttpClient) { }

  //Get Volunteers
  getVolunteers(): Observable<Volunteer[]>{
    return this.http.get<Volunteer[]>(`${this.volunteersUrl}`);
  }

  addVolunteer(volunteer: Volunteer): Observable<Volunteer>{
    return this.http.post<Volunteer>(this.volunteersUrl, volunteer, httpOptions);
  }
}
