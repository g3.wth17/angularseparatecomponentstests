import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Facilitator } from '../models/Facilitator';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class FacilitatorService {
  facilitatorsUrl: string = 'http://localhost:50568/api/facilitators';
  constructor(private http:HttpClient) { }

  getFacilitators(): Observable<Facilitator[]>{
    return this.http.get<Facilitator[]>(`${this.facilitatorsUrl}`);
  }

  addFacilitator(facilitator: Facilitator): Observable<Facilitator>{
    return this.http.post<Facilitator>(this.facilitatorsUrl, facilitator, httpOptions);
  }
}
