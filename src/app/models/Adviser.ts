import { DatePipe } from '@angular/common';

export class Adviser{
    id:number;
    name:string;
    lastName: string;
    birthDate: DatePipe;
    genre:number;
    subject:string;
    cellphoneNumber: number;
    emailAddress: string;
}