import { DatePipe } from '@angular/common';

export class Facilitator{
    id:number;
    name:string;
    lastName: string;
    birthDate: DatePipe;
    genre:number;
    country:string;
    cellphoneNumber: number;
    emailAddress: string;
    speciality: string;
}