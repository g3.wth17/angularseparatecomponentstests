import { DatePipe } from '@angular/common';

export class Volunteer{
    id:number;
    name:string;
    lastName: string;
    birthDate: DatePipe;
    genre:number;
    cellphoneNumber: number;
    emailAddress: string;
}