import { DatePipe } from '@angular/common';

export class Student{
    id:number;
    name:string;
    lastName: string;
    birthDate: DatePipe;
    genre: number;
    school:string;
    userName:string;
}