import { Pair } from './Pair';


export class ResourceContent{
    code: string;
    title: string;
    description:string;
    imageUrl: string;
    resources: Pair[];
}