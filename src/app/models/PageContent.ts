
export class PageContent{
    code:string;
    title: string;
    content: string;
    imageUrl: string;
}