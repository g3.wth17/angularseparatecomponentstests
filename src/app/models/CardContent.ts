export class CardContent{
    code:string;
    title:string;
    imageUrl: string;
    backgroundColor: string;
}